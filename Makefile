ERLANG_PATH = $(shell erl -eval 'io:format("~s", [lists:concat([code:root_dir(), "/erts-", erlang:system_info(version), "/include"])])' -s init stop -noshell)

all:
	gcc -g -O3 -Wall -fPIC \
	-I"$(ERLANG_PATH)" \
	-Ic_src/deps \
	-shared \
	-o priv/fast_rand.so \
	c_src/fast_rand.c \
	c_src/deps/pcg_basic.c

clean:
	rm -r "priv/fast_rand.so"