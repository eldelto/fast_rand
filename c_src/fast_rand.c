/*
 * PCG Random Number Generation for C.
 *
 * Copyright 2014 Melissa O'Neill <oneill@pcg-random.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * For additional information about the PCG random number generation scheme,
 * including its license and other licensing options, visit
 *
 *     http://www.pcg-random.org
 */

/*
 * This file was mechanically generated from tests/check-pcg32.c
 * gcc -g -O3 -Wall -fPIC -I/usr/lib/erlang/erts-10.1.1/include -Isrc -shared -o fast-rand.so fast-rand.c pcg_basic.c

 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <time.h>

#include "pcg_basic.h"

#include <erl_nif.h>


ErlNifResourceType *RNG_TYPE;

static pcg32_random_t * seed(int seed) {    
    pcg32_random_t *rng = malloc(sizeof(pcg32_random_t));
    int rounds = 3;

    // Seed
    pcg32_srandom_r(rng, seed, (intptr_t)&rounds);

    return rng;
}

static int get_rand(pcg32_random_t *rng, int upper_bound) {
    return pcg32_boundedrand_r(rng, upper_bound);
}


// This is called everytime a resource is deallocated (which happens when
// enif_release_resource is called and Erlang garbage collects the memory)
static void res_destructor(ErlNifEnv *env, void *res) {
}

static int load(ErlNifEnv *env, void **priv_data, ERL_NIF_TERM load_info) {
    int flags = ERL_NIF_RT_CREATE | ERL_NIF_RT_TAKEOVER;
    RNG_TYPE = enif_open_resource_type(env, NULL, "rng", res_destructor, flags, NULL);

    return 0;
}

static ERL_NIF_TERM seed_nif(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]) {
    int seed_value;
    enif_get_int(env, argv[0], &seed_value);    

    // Let's allocate the memory for a pcg32_random_t * pointer
    pcg32_random_t **rng_res = enif_alloc_resource(RNG_TYPE, sizeof(pcg32_random_t *));

    // Let's create rng and copy the memory where the pointer is stored
    pcg32_random_t *rng = seed(seed_value);
    memcpy((void *) rng_res, (void *) &rng, sizeof(pcg32_random_t *));

    // We can now make the Erlang term that holds the resource...
    ERL_NIF_TERM term = enif_make_resource(env, rng_res);
    // ...and release the resource so that it will be freed when Erlang garbage collects
    enif_release_resource(rng_res);

    return term;
}

static ERL_NIF_TERM get_rand_nif(ErlNifEnv *env, int argc, const ERL_NIF_TERM argv[]) {
    pcg32_random_t *rng;
    enif_get_resource(env, argv[0], RNG_TYPE, (void **) &rng);

    int upper_bound;
    enif_get_int(env, argv[1], &upper_bound);
    
    int rand = get_rand(rng, upper_bound);

    return enif_make_int(env, rand);
}


ErlNifFunc nif_funcs[] = 
{
    {"seed", 1, seed_nif},
    {"get_rand", 2, get_rand_nif},
};

ERL_NIF_INIT(Elixir.FastRand, nif_funcs, load, NULL, NULL, NULL);