defmodule FastRand do
  @on_load :init

  def init do
    path = :fast_rand
      |> :code.priv_dir()
      |> Path.join("fast_rand")
      |> String.to_charlist()
    :ok = :erlang.load_nif(path, 0)
  end

  def seed(_seed) do
    raise "NIF seed/1 not implemented"
  end

  def get_rand(_rng, _range) do
    raise "NIF get_rand/2 not implemented"
  end
end
