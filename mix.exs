defmodule Mix.Tasks.Compile.Nif do
  use Mix.Task.Compiler

  def run(_args) do
    File.mkdir_p("priv")
    {result, _errcode} = System.cmd("make", [])
    IO.binwrite(result)
  end
end

defmodule FastRand.MixProject do
  use Mix.Project

  def project do
    [
      app: :fast_rand,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      compilers: [:nif] ++ Mix.compilers(),
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    []
  end
end
